## Swaggers

**LoginService:** http://localhost:6060/swagger/index.html

**CatalogService:** http://localhost:6061/swagger/index.html

**CartService:** http://localhost:6062/swagger/index.html

**OrderService:** http://localhost:6063/swagger/index.html

## Test Credit Card

**Holder:** John Doe

**Number:** 4111111111111111

**Year:** 2026

**Month:** 12 

**Code:** 123