﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Identity
{
    public class IdentityResolver : IIdentityResolver
    {
        private IHttpContextAccessor _httpContextAccessor;

        public IdentityResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Guid GetIdentity()
        {
            var claim = _httpContextAccessor.HttpContext?.User.FindFirst(System.Security.Claims.ClaimTypes.Name);
            if (claim == null || string.IsNullOrWhiteSpace(claim.Value))
                return Guid.Empty;

            return Guid.Parse(claim.Value);
        }
    }
}
