﻿using System;

namespace Common.Identity
{
    public interface IIdentityResolver
    {
        Guid GetIdentity();
    }
}
