﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Configurations
{
    public class RabbitMQSettings : IRabbitMQSettings
    {
        public string Host { get; set; }
        public ushort Port { get; set; }
        public string VirtualHost { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public interface IRabbitMQSettings
    {
        string Host { get; set; }
        ushort Port { get; set; }
        string VirtualHost { get; set; }
        string Username { get; set; }
        string Password { get; set; }
    }
}
