﻿namespace Common.Configurations
{
    public class RedisSettings : IRedisSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }

    public interface IRedisSettings
    {
        string Host { get; set; }
        int Port { get; set; }
    }
}
