﻿using System;

namespace Common.Configurations
{
    public class JwtSettings : IJwtSettings
    {
        public Guid SecretKey { get; set; }
    }

    public interface IJwtSettings
    {
        Guid SecretKey { get; set; }
    }
}
