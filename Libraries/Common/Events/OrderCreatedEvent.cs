﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Events
{
    public class OrderCreatedEvent
    {
        public Guid UserId { get; set; }
        public Guid TransactionId { get; set; }
    }
}
