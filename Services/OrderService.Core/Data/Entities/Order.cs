﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Core.Data.Entities
{
    public class Order : BaseEntity
    {
        public Guid UserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal TotalPrice { get; set; }

        public List<OrderItem> Items { get; set; }
    }
}
