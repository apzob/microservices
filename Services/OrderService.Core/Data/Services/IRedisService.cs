﻿using StackExchange.Redis;
using System.Collections.Generic;

namespace OrderService.Core.Data.Services
{
    public interface IRedisService
    {
        void Connect();
        IDatabase GetDb(int db = 0);

        IEnumerable<RedisKey> GetKeys(string pattern);
    }
}
