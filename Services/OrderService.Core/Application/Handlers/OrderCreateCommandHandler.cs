﻿using MediatR;
using OrderService.Core.Application.Commands;
using OrderService.Core.Application.Models;
using OrderService.Core.Application.Responses;
using OrderService.Core.Data.Contexts;
using OrderService.Core.Data.Services;
using StackExchange.Redis;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService.Core.Application.Handlers
{
    public class OrderCreateCommandHandler : IRequestHandler<OrderCreateCommand, OrderCreateCommandResponse>
    {
        private readonly IRedisService _redisService;
        private readonly OrderDbContext _dbContext;
        public OrderCreateCommandHandler(IRedisService redisService, OrderDbContext orderDbContext)
        {
            _redisService = redisService;
            _dbContext = orderDbContext;
        }

        public async Task<OrderCreateCommandResponse> Handle(OrderCreateCommand request, CancellationToken cancellationToken)
        {
            var cartKey = new RedisKey($"Cart_{request.UserId}");
            var orderKey = new RedisKey($"Order_{request.UserId}");

            var db = _redisService.GetDb();

            var cartValue = await db.StringGetAsync(cartKey);
            if (cartValue.IsNullOrEmpty)
                return null;

            var cart = JsonSerializer.Deserialize<CartModel>(cartValue);
            if (cart == null)
                return null;

            var order = new Data.Entities.Order
            {
                Id = request.TransactionId,
                UserId = cart.UserId,
                CreatedDate = DateTime.Now,
                TotalPrice = cart.TotalPrice,
                Items = cart.Items
                            .Select(p => new Data.Entities.OrderItem
                            {
                                ProductId = p.ProductId,
                                ProductName = p.ProductName,
                                Price = p.Price,
                                Quantity = p.Quantity
                            }).ToList()
            };

            _dbContext.Orders.Add(order);

            var result = await _dbContext.SaveChangesAsync(cancellationToken);

            if (result > 0)
            {
                await db.KeyDeleteAsync(cartKey);
                await db.KeyDeleteAsync(orderKey);
            }
            
            return new OrderCreateCommandResponse
            {
                UserId = request.UserId,
                TransactionId = request.TransactionId,
                Created = result > 0
            };
        }
    }
}
