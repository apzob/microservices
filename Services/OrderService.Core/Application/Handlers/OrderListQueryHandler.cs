﻿using OrderService.Core.Application.Models;
using OrderService.Core.Application.Queries;
using OrderService.Core.Application.Responses;
using OrderService.Core.Data.Services;
using MediatR;
using StackExchange.Redis;
using System.Text.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using OrderService.Core.Data.Contexts;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace OrderService.Core.Application.Handlers
{
    public class OrderListQueryHandler : IRequestHandler<OrderListQuery, OrderListQueryResponse>
    {
        private readonly IRedisService _redisService;
        private readonly OrderDbContext _orderDbContext;

        public OrderListQueryHandler(IRedisService redisService, OrderDbContext orderDbContext)
        {
            _redisService = redisService;
            _orderDbContext = orderDbContext;
        }

        public async Task<OrderListQueryResponse> Handle(OrderListQuery request, CancellationToken cancellationToken)
        {
            var orderKey = new RedisKey($"Order_{request.UserId}");

            var db = _redisService.GetDb();

            var ordersValue = await db.StringGetAsync(orderKey);
            var orders = new List<OrderModel>();

            if (!ordersValue.IsNullOrEmpty)
            {
                orders = JsonSerializer.Deserialize<List<OrderModel>>(ordersValue);
            }
            else
            {
                orders = await _orderDbContext
                         .Orders
                         .Where(p => p.UserId == request.UserId)
                         .Select(p => new OrderModel
                         {
                             UserId = p.UserId,
                             TransactionId = p.Id,
                             CreatedDate = DateTime.Now,
                             TotalPrice = p.TotalPrice,
                             Items = p.Items.Select(i => new OrderItemModel
                                             {
                                                 ProductId = i.ProductId,
                                                 ProductName = i.ProductName,
                                                 Price = i.Price,
                                                 Quantity = i.Quantity
                                             }).ToList()
                         }).ToListAsync();

                await db.StringSetAsync(orderKey, JsonSerializer.Serialize(orders), expiry: TimeSpan.FromMinutes(5));
            }

            return new OrderListQueryResponse
            {
                Orders = orders
            };
        }
    }
}
