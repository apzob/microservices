﻿using OrderService.Core.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Core.Application.Queries
{
    public class OrderListQuery : IRequest<OrderListQueryResponse>
    {
        public Guid UserId { get; set; }
    }
}
