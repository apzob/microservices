﻿using OrderService.Core.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Core.Application.Responses
{
    public class OrderListQueryResponse
    {
        public List<OrderModel> Orders { get; set; }
    }
}
