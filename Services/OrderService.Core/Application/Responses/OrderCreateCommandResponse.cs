﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Core.Application.Responses
{
    public class OrderCreateCommandResponse
    {
        public Guid UserId { get; set; }
        public Guid TransactionId { get; set; }
        public bool Created { get; set; }
    }
}
