﻿using OrderService.Core.Application.Models;
using OrderService.Core.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace OrderService.Core.Application.Commands
{
    public class OrderCreateCommand : IRequest<OrderCreateCommandResponse>
    {
        [JsonIgnore]
        public Guid UserId { get; set; }
        public Guid TransactionId { get; set; }
    }
}
