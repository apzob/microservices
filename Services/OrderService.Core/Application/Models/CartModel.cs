﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderService.Core.Application.Models
{
    public class CartModel
    {
        public Guid UserId { get; set; }

        public List<CartItemModel> Items { get; set; }

        public decimal TotalPrice
        {
            get
            {
                return Items.Sum(x => x.Price * x.Quantity);
            }
        }
    }
}
