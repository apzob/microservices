﻿using System;
using System.Collections.Generic;

namespace OrderService.Core.Application.Models
{
    public class OrderModel
    {
        public Guid TransactionId { get; set; }
        public Guid UserId { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<OrderItemModel> Items { get; set; }
    }
}
