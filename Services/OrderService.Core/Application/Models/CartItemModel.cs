﻿using System;

namespace OrderService.Core.Application.Models
{
    public class CartItemModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
