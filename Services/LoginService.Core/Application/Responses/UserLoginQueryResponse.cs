﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginService.Core.Application.Responses
{
    public class UserLoginQueryResponse
    {
        public string AccessToken { get; set; }
    }
}
