﻿using LoginService.Core.Application.Queries;
using LoginService.Core.Application.Responses;
using Common.Configurations;
using LoginService.Core.Data.Contexts;
using LoginService.Core.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace LoginService.Core.Application.Handlers
{
    public class UserLoginQueryHandler : IRequestHandler<UserLoginQuery, UserLoginQueryResponse>
    {
        private readonly LoginDbContext _dbContext;
        private readonly IJwtSettings _jwtSettings;

        public UserLoginQueryHandler(LoginDbContext dbContext, IJwtSettings jwtSettings)
        {
            _dbContext = dbContext;
            _jwtSettings = jwtSettings;
        }

        public async Task<UserLoginQueryResponse> Handle(UserLoginQuery request, CancellationToken cancellationToken)
        {
            var hashedPassword = request.Password.HashSHA256();

            var user = await _dbContext.Users
                             .Where(p => p.Email == request.Email && p.Password == hashedPassword)
                             .FirstOrDefaultAsync();

            if (user == null)
                return null;

            string accessToken = CreateJwtToken(user.Id, DateTime.UtcNow.AddDays(1));
            
            return new UserLoginQueryResponse
            {
                AccessToken = accessToken
            };
        }

        private string CreateJwtToken(Guid userId, DateTime expires)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.SecretKey.ToString());
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                        new Claim(ClaimTypes.Name, userId.ToString())
                }),
                Expires = expires,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenCreator = tokenHandler.CreateToken(tokenDescriptor);

            var token = tokenHandler.WriteToken(tokenCreator);
            return token;
        }
    }
}
