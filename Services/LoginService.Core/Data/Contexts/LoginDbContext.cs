﻿using LoginService.Core.Data.Entities;
using LoginService.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginService.Core.Data.Contexts
{
    public class LoginDbContext : DbContext
    {
        public LoginDbContext(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User 
                { 
                    Id = Guid.Parse("ea1471f6-bfe4-4994-9497-ac9725913ea5"), 
                    FirstName = "User", 
                    LastName = "One", 
                    Email = "user.one@user.com", 
                    Password = "user.one".HashSHA256()
                },
                new User
                {
                    Id = Guid.Parse("c06020c5-7891-4c39-8171-2991450e8115"),
                    FirstName = "User",
                    LastName = "Two",
                    Email = "user.two@user.com",
                    Password = "user.two".HashSHA256()
                }
            );

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }
    }
}
