﻿using Common.Events;
using MassTransit;
using MediatR;
using OrderService.Core.Application.Commands;
using System.Threading.Tasks;

namespace OrderService.Consumers
{
    public class OrderCreatedEventConsumer : IConsumer<OrderCreatedEvent>
    {
        private readonly IMediator _mediator;

        public OrderCreatedEventConsumer(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Consume(ConsumeContext<OrderCreatedEvent> context)
        {
            var command = new OrderCreateCommand 
            { 
                UserId = context.Message.UserId,
                TransactionId = context.Message.TransactionId
            };

            await _mediator.Send(command);
        }
    }
}
