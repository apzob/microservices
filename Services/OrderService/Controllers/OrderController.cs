using OrderService.Core.Application.Queries;
using Common.Identity;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace OrderService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IIdentityResolver _identityResolver;

        public OrderController(IMediator mediator, IIdentityResolver identityResolver)
        {
            _mediator = mediator;
            _identityResolver = identityResolver;
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> List()
        {
            var query = new OrderListQuery
            {
                UserId = _identityResolver.GetIdentity()
            };

            var response = await _mediator.Send(query);
            if (response == null)
                return NotFound();

            return Ok(response);
        }
    }
}