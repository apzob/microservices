﻿using StackExchange.Redis;

namespace CartService.Core.Data.Services
{
    public interface IRedisService
    {
        void Connect();
        IDatabase GetDb(int db = 0);
    }
}
