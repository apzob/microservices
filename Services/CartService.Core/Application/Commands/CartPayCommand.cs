﻿using CartService.Core.Application.Responses;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CartService.Core.Application.Commands
{
    public class CartPayCommand : IRequest<CartPayCommandResponse>
    {
        [JsonIgnore]
        public Guid UserId { get; set; }

        [Required]
        public string Holder { get; set; }

        [Required]
        [CreditCard]
        public string Number { get; set; }

        [Required]
        [Range(2022, 2032)]
        public int Year { get; set; }

        [Required]
        [Range(1, 12)]
        public int Month { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(4)]
        public string Code { get; set; }
    }
}
