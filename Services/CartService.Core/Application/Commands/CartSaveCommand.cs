﻿using CartService.Core.Application.Models;
using CartService.Core.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CartService.Core.Application.Commands
{
    public class CartSaveCommand : IRequest<CartSaveCommandResponse>
    {
        [JsonIgnore]
        public Guid UserId { get; set; }
        public List<CartItemModel> Items { get; set; }
    }
}
