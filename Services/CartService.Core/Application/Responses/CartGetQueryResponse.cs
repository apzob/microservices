﻿using CartService.Core.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartService.Core.Application.Responses
{
    public class CartGetQueryResponse
    {
        public CartModel Cart { get; set; }
    }
}
