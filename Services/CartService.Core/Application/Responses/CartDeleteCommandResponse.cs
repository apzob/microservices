﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartService.Core.Application.Responses
{
    public class CartDeleteCommandResponse
    {
        public Guid Id { get; set; }
        public bool Deleted { get; set; }
    }
}
