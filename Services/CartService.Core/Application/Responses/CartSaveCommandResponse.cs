﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartService.Core.Application.Responses
{
    public class CartSaveCommandResponse
    {
        public Guid Id { get; set; }
        public bool Saved { get; set; }
    }
}
