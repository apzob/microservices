﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartService.Core.Application.Responses
{
    public class CartPayCommandResponse
    {
        public Guid Id { get; set; }
        public bool Paid { get; set; }
        public Guid TransactionId { get; set; }
    }
}
