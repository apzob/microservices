﻿using CartService.Core.Application.Commands;
using CartService.Core.Application.Responses;
using CartService.Core.Data.Services;
using MediatR;
using StackExchange.Redis;
using System.Threading;
using System.Threading.Tasks;

namespace CartService.Core.Application.Handlers
{
    public class CartDeleteCommandHandler : IRequestHandler<CartDeleteCommand, CartDeleteCommandResponse>
    {
        private readonly IRedisService _redisService;

        public CartDeleteCommandHandler(IRedisService redisService)
        {
            _redisService = redisService;
        }

        public async Task<CartDeleteCommandResponse> Handle(CartDeleteCommand request, CancellationToken cancellationToken)
        {
            var key = new RedisKey($"Cart_{request.UserId}");

            var deleted = await _redisService
                                .GetDb()
                                .KeyDeleteAsync(key);

            return new CartDeleteCommandResponse
            {
                Id = request.UserId,
                Deleted = deleted
            };
        }
    }
}
