﻿using CartService.Core.Application.Models;
using CartService.Core.Application.Commands;
using CartService.Core.Application.Responses;
using CartService.Core.Data.Services;
using MediatR;
using StackExchange.Redis;
using System.Text.Json;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace CartService.Core.Application.Handlers
{
    public class CartSaveCommandHandler : IRequestHandler<CartSaveCommand, CartSaveCommandResponse>
    {
        private readonly IRedisService _redisService;

        public CartSaveCommandHandler(IRedisService redisService)
        {
            _redisService = redisService;
        }

        public async Task<CartSaveCommandResponse> Handle(CartSaveCommand request, CancellationToken cancellationToken)
        {
            if (request.Items == null || request.Items.Count == 0)
                return null;

            var cart = new CartModel
            {
                UserId = request.UserId,
                Items = request.Items
            };

            var key = new RedisKey($"Cart_{request.UserId}");
            var saved = await _redisService
                              .GetDb()
                              .StringSetAsync(key, JsonSerializer.Serialize(cart), expiry: TimeSpan.FromMinutes(30));

            return new CartSaveCommandResponse
            {
                Id = request.UserId,
                Saved = saved
            };
        }
    }
}
