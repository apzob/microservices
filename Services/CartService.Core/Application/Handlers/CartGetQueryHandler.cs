﻿using CartService.Core.Application.Models;
using CartService.Core.Application.Queries;
using CartService.Core.Application.Responses;
using CartService.Core.Data.Services;
using MediatR;
using StackExchange.Redis;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace CartService.Core.Application.Handlers
{
    public class CartGetQueryHandler : IRequestHandler<CartGetQuery, CartGetQueryResponse>
    {
        private readonly IRedisService _redisService;

        public CartGetQueryHandler(IRedisService redisService)
        {
            _redisService = redisService;
        }

        public async Task<CartGetQueryResponse> Handle(CartGetQuery request, CancellationToken cancellationToken)
        {
            var key = new RedisKey($"Cart_{request.UserId}");

            var value = await _redisService.GetDb().StringGetAsync(key);
            if (value.IsNullOrEmpty)
                return null;

            var cart = JsonSerializer.Deserialize<CartModel>(value);
            return new CartGetQueryResponse
            {
                Cart = cart,
            };
        }
    }
}
