﻿using CartService.Core.Application.Models;
using CartService.Core.Application.Commands;
using CartService.Core.Application.Responses;
using CartService.Core.Data.Services;
using MediatR;
using StackExchange.Redis;
using System.Text.Json;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace CartService.Core.Application.Handlers
{
    public class CartPayCommandHandler : IRequestHandler<CartPayCommand, CartPayCommandResponse>
    {
        private readonly IRedisService _redisService;

        public CartPayCommandHandler(IRedisService redisService)
        {
            _redisService = redisService;
        }

        public async Task<CartPayCommandResponse> Handle(CartPayCommand request, CancellationToken cancellationToken)
        {
            var key = new RedisKey($"Cart_{request.UserId}");

            var value = await _redisService.GetDb().StringGetAsync(key);
            if (value.IsNullOrEmpty)
                return null;

            var cart = JsonSerializer.Deserialize<CartModel>(value);
            if (cart == null || cart.TotalPrice == decimal.Zero)
                return null;

            var transactionId = Guid.NewGuid();
            var paid = Pay(cart, request, transactionId);

            return new CartPayCommandResponse
            {
                Id = request.UserId,
                TransactionId = transactionId,
                Paid = paid
            };
        }

        private bool Pay(CartModel cart, CartPayCommand paymentInfo, Guid transactionId)
        {
            //TODO: charge total price
            return true;
        }
    }
}
