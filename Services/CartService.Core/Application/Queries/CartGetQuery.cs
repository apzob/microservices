﻿using CartService.Core.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartService.Core.Application.Queries
{
    public class CartGetQuery : IRequest<CartGetQueryResponse>
    {
        public Guid UserId { get; set; }
    }
}
