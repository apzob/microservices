﻿using CatalogService.Core.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatalogService.Core.Data.Contexts
{
    public class CatalogDbContext : DbContext
    {
        public CatalogDbContext(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasData(
                new Product
                { 
                    Id = Guid.Parse("e3d84e94-dc32-4b70-add8-575509058c3d"), 
                    Name = "Product One",
                    ShortDescription = "Product One Short Description",
                    Price = 10
                },
                new Product
                {
                    Id = Guid.Parse("c98e9cc2-ccd4-4e5c-a768-7bbb89ab6d6d"),
                    Name = "Product Two",
                    ShortDescription = "Product Two Short Description",
                    Price = 20
                },
                new Product
                {
                    Id = Guid.Parse("a19c3906-489d-4a63-a2fc-626db4aa5d92"),
                    Name = "Product Three",
                    ShortDescription = "Product Three Short Description",
                    Price = 30
                },
                new Product
                {
                    Id = Guid.Parse("db23314a-8191-4908-9160-e1496816146d"),
                    Name = "Product Four",
                    ShortDescription = "Product Four Short Description",
                    Price = 40
                },
                new Product
                {
                    Id = Guid.Parse("43442062-6499-483b-9206-ad5c4872d592"),
                    Name = "Product Five",
                    ShortDescription = "Product Five Short Description",
                    Price = 50
                }
            );

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Product> Products { get; set; }
    }
}
