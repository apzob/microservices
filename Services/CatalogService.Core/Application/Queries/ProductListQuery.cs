﻿using CatalogService.Core.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatalogService.Core.Application.Queries
{
    public class ProductListQuery : IRequest<ProductListQueryResponse>
    {
        public string Name { get; set; }
    }
}
