﻿using CatalogService.Core.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatalogService.Core.Application.Commands
{
    public class ProductCreateCommand : IRequest<ProductCreateCommandResponse>
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string ShortDescription { get; set; }

        [Required]
        public decimal Price { get; set; }
    }
}
