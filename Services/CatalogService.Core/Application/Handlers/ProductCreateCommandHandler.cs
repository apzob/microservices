﻿using CatalogService.Core.Application.Queries;
using CatalogService.Core.Application.Responses;
using Common.Configurations;
using CatalogService.Core.Data.Contexts;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CatalogService.Core.Application.Commands;
using CatalogService.Core.Data.Entities;
using Common.Identity;
using System.Threading;

namespace CatalogService.Core.Application.Handlers
{
    public class ProductCreateCommandHandler : IRequestHandler<ProductCreateCommand, ProductCreateCommandResponse>
    {
        private readonly CatalogDbContext _dbContext;
        private readonly IIdentityResolver _identityResolver;

        public ProductCreateCommandHandler(CatalogDbContext dbContext, IIdentityResolver identityResolver)
        {
            _dbContext = dbContext;
            _identityResolver = identityResolver;
        }

        public async Task<ProductCreateCommandResponse> Handle(ProductCreateCommand request, CancellationToken cancellationToken)
        {
            var product = new Product
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                ShortDescription = request.ShortDescription,
                Price = request.Price,
                CreatedBy = _identityResolver.GetIdentity()
            };

            _dbContext.Products.Add(product);

            var result = await _dbContext.SaveChangesAsync(cancellationToken);

            return new ProductCreateCommandResponse
            {
                Id = product.Id,
                Created = result > 0
            };
        }
    }
}
