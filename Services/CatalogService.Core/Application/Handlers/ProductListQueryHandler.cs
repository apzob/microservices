﻿using CatalogService.Core.Application.Queries;
using CatalogService.Core.Application.Responses;
using Common.Configurations;
using CatalogService.Core.Data.Contexts;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CatalogService.Core.Application.Models;
using System.Threading;

namespace CatalogService.Core.Application.Handlers
{
    public class ProductListQueryHandler : IRequestHandler<ProductListQuery, ProductListQueryResponse>
    {
        private readonly CatalogDbContext _dbContext;

        public ProductListQueryHandler(CatalogDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ProductListQueryResponse> Handle(ProductListQuery request, CancellationToken cancellationToken)
        {
            var query = _dbContext.Products.AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.Name))
                query = query.Where(p => p.Name.Contains(request.Name));

            var products = await query
                .Select(p => new ProductModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    ShortDescription = p.ShortDescription,
                    Price = p.Price
                }).ToListAsync();

            return new ProductListQueryResponse
            {
                Products = products,
            };
        }
    }
}
