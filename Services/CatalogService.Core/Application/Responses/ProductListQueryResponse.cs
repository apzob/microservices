﻿using CatalogService.Core.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatalogService.Core.Application.Responses
{
    public class ProductListQueryResponse
    {
        public List<ProductModel> Products { get; set; }
    }
}
