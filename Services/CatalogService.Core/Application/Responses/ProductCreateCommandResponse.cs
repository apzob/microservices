﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatalogService.Core.Application.Responses
{
    public class ProductCreateCommandResponse
    {
        public Guid Id { get; set; }
        public bool Created { get; set; }
    }
}
