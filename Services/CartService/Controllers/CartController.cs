﻿using CartService.Core.Application.Commands;
using CartService.Core.Application.Queries;
using Common.Events;
using Common.Identity;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CartService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CartController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IIdentityResolver _identityResolver;
        private readonly IPublishEndpoint _publishEndpoint;

        public CartController(IMediator mediator, IIdentityResolver identityResolver, IPublishEndpoint publishEndpoint)
        {
            _mediator = mediator;
            _identityResolver = identityResolver;
            _publishEndpoint = publishEndpoint;
        }

        [HttpGet]
        [Route("get")]
        public async Task<IActionResult> Get()
        {
            var query = new CartGetQuery 
            { 
                UserId = _identityResolver.GetIdentity()
            };

            var response = await _mediator.Send(query);
            if (response == null)
                return NotFound();

            return Ok(response);
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Save(CartSaveCommand command)
        {   
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            command.UserId = _identityResolver.GetIdentity();

            var response = await _mediator.Send(command);
            if (response == null)
                return BadRequest("Empty Cart");

            return Ok(response);
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<IActionResult> Delete()
        {
            var command = new CartDeleteCommand
            {
                UserId = _identityResolver.GetIdentity()
            };

            var response = await _mediator.Send(command);
            return Ok(response);
        }

        [HttpPost]
        [Route("pay")]
        public async Task<IActionResult> Pay(CartPayCommand command)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            command.UserId = _identityResolver.GetIdentity();

            var response = await _mediator.Send(command);
            if (response == null)
                return BadRequest();

            if (response.Paid)
            {
                var orderCreatedEvent = new OrderCreatedEvent
                {
                    UserId = response.Id,
                    TransactionId = response.TransactionId
                };

                await _publishEndpoint.Publish(orderCreatedEvent);
            }

            return Ok(response);
        }
    }
}
